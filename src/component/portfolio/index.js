import Arrow from '../Common/Arrow';
import ArtSvg from '../Common/ArtSvg';
import Border from '../Common/BorderBox';
import Circle from '../Common/Circle';
import CodeSvg from '../Common/Code';
import Digger from '../Common/Digger';
import FigmaSvg from '../Common/Figma';
import GamePad from '../Common/GamePad';
import Github from '../Common/Github';
import LineFlow from '../Common/LineFlow';
import LinkedIn from '../Common/LinkedIn';
import Logo from '../Common/Logo';
import Mail from '../Common/Mail';
import MeetMeLine from '../Common/MeetMeLine';
import MouseClickSvg from '../Common/MouseClick';
import ReactSvg from '../Common/React';
import TSvg from '../Common/TSvg';
import Twitter from '../Common/Twitter';
import './style.css';
function Portfolio(){
    return(
    <div className='main-section' id='main-section'>
        <div className='header'>
            <Logo />
            {/* <p className='location'>CHENNAI INDIA</p> */}
            </div>
        <div>
            <Circle />
            <div className='des'>
                <h1 className='title'>Vishal TK</h1>
                <span className="underline"></span>
                <p className='role'>Designer & Developer</p>
                <p className='content'>I build, craft and experiment products & games in the intersection of design, art and programming through beautiful and simplistic UX</p>
                <div className='line-arrow'>
                <LineFlow />
                <div className='meet-me'>
                    <p className='meet-me-text'> Meet Me</p>
                    <Arrow />
                    </div>
                </div>
            </div>
            <div className='profile'>
                <TSvg />
                <ReactSvg />
                <FigmaSvg />
                <CodeSvg />
                <GamePad />
                <MouseClickSvg />
                <ArtSvg />
                

                <img src='https://res.cloudinary.com/dnoajknet/image/upload/v1648021065/me_r24gqd.png' className='img' />
            <Border />
            </div>
        </div>
     <MeetMeLine />

        <div className='footer'>
            <p className='stright-line'></p>
            <div className='social-media'>
            <Github />
            <Twitter />
            <Digger />
            <LinkedIn />
            <Mail />
            </div>
       
         

        </div>
    </div>
    )
}

export default Portfolio;