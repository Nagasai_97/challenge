function CodeSvg(){
    return(
        <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg" className="code-svg">
<path d="M16.6485 18.3878L22.6485 12.3878L16.6485 6.38782" stroke="#ff99cc" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M8.6485 6.38782L2.6485 12.3878L8.6485 18.3878" stroke="#ff99cc" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
</svg>

    )
}

export default CodeSvg;