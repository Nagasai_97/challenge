function Mail(){
    return (
        <svg width="22" height="22" viewBox="0 0 22 22" fill="none" stroke="" xmlns="http://www.w3.org/2000/svg">
<path d="M4 4H18C18.9625 4 19.75 4.7875 19.75 5.75V16.25C19.75 17.2125 18.9625 18 18 18H4C3.0375 18 2.25 17.2125 2.25 16.25V5.75C2.25 4.7875 3.0375 4 4 4Z" stroke="#c94d62" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M19.75 5.75L11 11.875L2.25 5.75" stroke="#c94d62" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
</svg>

    )
}

export default Mail;